//Globabl variable for tracking reloads to index
var pages = ["index.html", "calendar.html", "directory.html", "directions.html", "bus.html"];
var idleTime = 0;

/**
	Auto generated stub from google analytics.
**/
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	//Do not send tracking info for the timeout that returns us to the homepage.		
	//if(window.location.href.indexOf('?pg=sleep') == -1){
	  ga('create', 'UA-73964028-4', 'auto');
	  ga('send', 'pageview');
	//}

/**
	Page Load Actions - Called by every full page on body load
**/
function loadPage(){
	//Load header, footer, and of necessary the maps
	$("#uwecheader").load("./templates/header.html"); 
	$("#uwecfooter").load("./templates/footer.html"); 
	if($("#modalMaps")){
		$("#modalMaps").load("./modalMaps.html"); 
	}

	//Load the weather data and call the loader func
	$.get("https://ipinfo.io", function(response) {		
		$('#city').text(response.city);
		latlon = response.loc.split(",")
		getWeather(latlon[0], latlon[1]);
	}, "jsonp");
	
	//Increment our timer Source: http://stackoverflow.com/questions/667555/detecting-idle-time-in-javascript-elegantly		
	$(document).ready(function () {
		//Increment the idle time counter every minute.
		var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

		//Zero the idle timer on mouse movement.
		$(this).mousemove(function (e) {
			idleTime = 0;
		});
		$(this).keypress(function (e) {
			idleTime = 0;
		});
	});	
}

//Increment timer, if its passed the time limit change to a random page for display mode
function timerIncrement() {
	idleTime = idleTime + 1;	
	if (idleTime > 5) 
	{ //5 minutes
		var pageIndex = Math.floor((Math.random() * (pages.length)));
		if(window.location.href.indexOf(pages[pageIndex]) == -1)
		{
			window.location = "./" + pages[pageIndex] + "?pg=sleep&index=" + pageIndex;
		}		
	}
}

//Gather weather data and load into proper divs for display
function getWeather(lat, lon){
	$.get("https://forecast.weather.gov/MapClick.php?lat=" + lat + "&lon=" + lon + "&FcstType=json", function(weather) {		
		$('#temp-text').text(Math.round(weather.currentobservation.Temp) + "F")	
		$('#sky-text').text(weather.currentobservation.Weather)
		$('#wind-text').text("Wind:" + Math.round(weather.currentobservation.Winds) + " mph")
		;
		//$('#curr-image').attr('src', 'http://www.nws.noaa.gov/weather/images/fcicons/' + weather.currentobservation.Weatherimage.replace("png","jpg"));
	}, "jsonp");  
}
@echo off
set t=%time:~0,8%
set t=%t::=-%
set t=%t: =%
set d=%date%
set d=%d:/=-%
set d=%d: =-%
set FileName=Build-%d%(%t%)

@echo on
robocopy ./prod/ ./backups/%FileName% /e /v
@echo off

echo Hope it looks good...
pause